import pytest
import sys
from pytest_mock import mocker
from json import load
from pathlib import Path


base_path = Path(__file__).parent
sys.path.append(base_path.parent)
print(f'CWD: {Path.cwd()}')
print(f'BASE_PATH: {base_path}')

from autoresponder.main import Notification


@pytest.fixture
def notification():
    fixtures_dir = base_path / 'fixtures'
    notification_path = fixtures_dir / 'notification.json'
    with open(notification_path, 'r', encoding='utf8') as fl:
        return load(fl)


def test_notifi_object(notification):
    obj = Notification(notification, [])
    assert notification['account'] == obj.account
    assert notification['sender'] == obj.sender
    assert notification['id'] == obj.id
    assert notification['is_reply'] == obj.is_reply
    assert notification['is_sender_in_followers'] == obj.is_sender_in_followers
    assert notification['mentioned_accounts'] == obj.mentioned_accounts
    assert notification['status_id'] == obj.status_id
    assert notification['status_content'] == obj.status_content
    assert notification['visibility'] == obj.visibility
    assert notification['type'] == obj.type
