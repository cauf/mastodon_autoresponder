FROM python:3.9-alpine

COPY ./src/autoresponder ./requirements.pip ./config.yaml /home/bot/

RUN apk add build-base && \
    pip install --no-cache-dir -r /home/bot/requirements.pip && \
    apk del build-base

WORKDIR /home/bot

CMD ["python", "main.py"]
