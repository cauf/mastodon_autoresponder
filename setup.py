from setuptools import find_packages
from setuptools import setup

setup(
    name="mastodon-autoresponder",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    platforms="any",
    python_requires=">=3.6",
    license="MIT",
    author="cauf",
    author_email="caufman@yandex.ru",
    description="",
    long_description=open("README.md", encoding="utf-8").read(),
    keywords="mastodon fediverse bot",
    classifiers=[
        "Development Status :: 5 - Testing",
        "Framework :: Pytest",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3 :: Only",
    ],
)