import time

from math import ceil
from datetime import datetime as dt
from typing import Dict, Any, List, Set, Iterator
from mastodon import Mastodon
from settings import (
    log,
    BOT_ADMINS,
    BOT_MESSAGE,
    BOT_WELCOME,
    BOT_LANG,
    COUNT_LIMIT,
    LOOP_TIMEOUT,
    TOOT_LEN,
)
from utils import (
    APICallException,
    get_api,
    get_cache,
    html_to_text,
    read_last_id,
    write_last_id,
)


class Notification:
    def __init__(self, data: Dict[str,Any], followers: List[str]) -> None:
        self.account:str = data['account']['acct']
        self.sender:str = data['status']['account']['acct']
        self.id:int = int(data['id'])
        self.is_reply:bool = data['status']['in_reply_to_id'] is not None
        self.is_sender_in_followers:bool = self.sender in followers
        self.mentioned_accounts:Set[str] = {
            account['acct'] 
            for account in data['status']['mentions']
        }
        self.status_id:int = int(data['status']['id'])
        self.status_content = html_to_text(
            data['status']['content']
        ).replace('@', '/')
        self.visibility:str = data['status']['visibility']
        self.type:str = data['type']

    def get_parted_content(self, prefix:str) -> Iterator[str]:
        """
        Разделение длинного текста на отдельные сообщения, не превышающие по длине
        указанного в настройке TOOT_LEN лимита

        :param prefix: Префикс в виде перечисления всех упомянутых аккаунтов
        :type prefix: str
        """    
        part_len = TOOT_LEN - len(prefix) - 3
        subtoots_count = ceil(len(self.status_content) / part_len)

        for i in range(subtoots_count):
            start = part_len*i
            end = part_len*(i+1)
            subtoot_text = self.status_content[start:end]
            subtoot = f'{prefix}\n{subtoot_text}'

            if end < len(self.status_content):
                subtoot += '\n…'

            yield subtoot


def send_welcome(notification:Notification, api:Mastodon) -> None:
    """
    Если тип уведомления "подписка", то отправляется сообщение с приветствием

    :param notification: Объект с информацией об уведомлении
    :type notification: Notification
    :param api: API для взаимодействия с Mastodon
    :type api: Mastodon
    """
    if notification.type == 'follow':
        log.info(f'Welcomed new follower: {notification.account}')
        response_sent = api.status_post(
            f'{BOT_WELCOME} @{notification.account}',
            visibility='direct',
            language=BOT_LANG,
        )


def processing_notification_from_follower(
    notification:Notification, 
    api:Mastodon
) -> None:
    """
    Обработка уведомления в случае, если отправитель является подписчиком

    :param notification: Объект с информацией уведомления
    :type notification: Notification
    :param api: API обращения к Mastodon
    :type api: Mastodon
    """    
    # если пост публичный или не является ответом на другой пост
    # то пропускаем обработку
    if (
        (notification.visibility == 'public')
        and
        (not notification.is_reply)
    ):
        # шарим сообщение
        api.status_reblog(notification.status_id)
        log.info(
            f"Responded to status {notification.status_id}"
            f" from {notification.sender}"
        )


def processing_other_notification(
    notification:Notification, 
    api:Mastodon
) -> None:
    """
    Обработка уведомления в остальных случаях

    :param notification: Объект с информацией уведомления
    :type notification: Notification
    :param api: API обращения к Mastodon
    :type api: Mastodon
    """
    # если в упомянутых есть админы бота 
    # то пропускаем дальнейшую обработку
    if set(BOT_ADMINS) & notification.mentioned_accounts:
        # админы всегда упомянуты - нет смысла передавать сообщение
        log.info(
            f"User {notification.sender}"
            " is already in admins list"
        )
        return
    
    response_sent = api.status_post(
        f'@{notification.sender} {BOT_MESSAGE}',
        in_reply_to_id=notification.status_id,
        visibility='direct',
        language=BOT_LANG,
    )

    # если видимость поста не публичная и количество указанных админов
    # больше 0 
    # то рассылаем сообщение всем админам и отправителю
    if (
        (notification.visibility != 'public')
        and
        (len(BOT_ADMINS) > 0)
    ):
        send_to_admins(notification, api, response_sent)


def send_to_admins(
    notification:Notification, 
    api:Mastodon,
    response_sent: Dict[str, Any],
) -> None:
    """
    Рассылка сообщения всем админам бота и отправителю

    :param notification: Объект с информацией уведомления
    :type notification: Notification
    :param api: API обращения к Mastodon
    :type api: Mastodon
    :param response_sent: Ответ API с результатами отправки сообщения
    :type response_sent: Dict[str, Any]
    """    
    recipients = [*BOT_ADMINS, notification.sender]
    recipient_prefix = ' '.join([f'@{r}' for r in recipients])
    prev_part_id = response_sent['id']
    for part in notification.get_parted_content(recipient_prefix):
        part_sent = api.status_post(
            part,
            in_reply_to_id=prev_part_id,
            visibility='direct',
            language=BOT_LANG,
        )
        prev_part_id = part_sent['id']


def main() -> None:
    api = get_api()
    cache = get_cache(0)
    last_id:int = None

    # данные моего аккаунта
    my_account = api.account_verify_credentials()

    while True:

        log.info('Start loop')

        # если last_id не существует, то считываем его
        if not last_id:
            last_id = read_last_id()

        # получение всех оповещений
        all_notifications_data = api.notifications()
        # from settings import BASE_PATH
        # from json import dumps
        # with open(BASE_PATH / 'out.txt', 'w', encoding='utf-8') as of:
        #     of.write(
        #         dumps(
        #             all_notifications_data,
        #             ensure_ascii=False,
        #             indent=4,
        #             default=str,
        #         )
        #     )
        # break
        log.debug(f'Get {len(all_notifications_data)} notifications')

        # список аккаунтов подписчиков
        followers_full_data = api.fetch_remaining(
            api.account_followers(
                my_account.id, 
                limit=60,
            )
        )
        followers=[fl.acct for fl in followers_full_data]
        log.debug(f'Get {len(followers)} followers')

        if (
            isinstance(all_notifications_data, dict) 
            and 
            ('error' in all_notifications_data)
        ):
            log.error(all_notifications_data['error'])
            raise APICallException(all_notifications_data['error'])
        
        # обратный (от старых к новым) перебор полученных уведомлений
        for notification_data in all_notifications_data[::-1]:

            notification = Notification(notification_data, followers)
            log.debug(
                f'Start processing a notification with id={notification.id}'
            )

            # если id текущего уведомления меньше либо равен last_id, 
            # то переходим к следующем без обновления значения last_id
            if notification.id <= last_id:
                log.info(
                    f"The current notification has id {notification.id} less"
                    f" than the last notification with id {last_id}"
                )
                continue

            # если текущее уведомление имеет непустое поле status.in_reply_to_id
            # то пропускаем - это ответ
            if notification.id <= last_id:
                log.info(
                    f"The current notification has id {notification.id} less"
                    f" than the last notification with id {last_id}"
                )
                continue

            # если для данного отправителя в кэше хранится три записи, 
            # то пропускаем дальнейшую обработку
            senders_notifi_list = cache.keys(f'{notification.sender}\t*')
            log.debug(str(senders_notifi_list))
            senders_count = len(senders_notifi_list)
            if senders_count >= COUNT_LIMIT: 
                log.info(
                    "The current notification with the identifier "
                    f"{notification.id} from the sender {notification.sender} "
                    "exceeds the maximum number of messages "
                    f"from one sender {COUNT_LIMIT}."
                )
                continue

            # приветствие новых подписчиков
            send_welcome(notification, api)

            log.info(
                f"Received status {notification.status_id}"
                f" from {notification.sender}."
            )

            # если отправитель в подписчиках
            if notification.is_sender_in_followers:
               processing_notification_from_follower(notification, api)
            # если отправитель не в подписчиках
            else:
                processing_other_notification(notification, api)
            
            # обновление значения последнего обработанного уведомления
            last_id = notification.id
            write_last_id(last_id)
            cache[f'{notification.sender}\t{dt.now().isoformat()}'] = notification.id

        log.info(f'Timeout {LOOP_TIMEOUT} secs.')
        time.sleep(LOOP_TIMEOUT)


if __name__ == '__main__':
    main()
