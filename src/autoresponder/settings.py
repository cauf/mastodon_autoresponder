from genericpath import isfile
import logging
import sys

from logging import basicConfig, getLogger
from yaml import load, FullLoader
from pathlib import Path


DEBUG = True


BASE_PATH = Path(__file__).parent if DEBUG else Path(__file__).parent
print(f"#=> Base path: {BASE_PATH}")

CONFIG_FILENAME = 'config.yaml'
CONFIG_PATH = BASE_PATH / CONFIG_FILENAME
if CONFIG_PATH.exists():
    print(f"#=> Existing config path: {CONFIG_PATH}")
else:
    print(f"#=> ERROR!!! Non-existent config path: {CONFIG_PATH}")


# read config
with open(CONFIG_PATH, 'r', encoding='utf-8') as config_file:
    _raw_data = config_file.read()
_data = load(_raw_data, FullLoader)


BASE_URL:str = _data['general']['base_url']
TOOT_LEN:int = int(_data['general']['toot_len'])
LAST_ID_FILENAME:str = _data['general']['last_id_filename']
LAST_ID_DIR:Path = BASE_PATH / 'last_id.d'
if not(LAST_ID_DIR.exists() and LAST_ID_DIR.is_dir()):
    LAST_ID_DIR.mkdir(parents=True)
LAST_ID_PATH:Path = LAST_ID_DIR / LAST_ID_FILENAME
LOOP_TIMEOUT:int = int(_data['general']['loop_timeout'])
COUNT_LIMIT:int = int(_data['general']['count_limit'])

AUTH_CLIENT_ID:str = _data['auth']['client_id']
AUTH_CLIENT_SECRET:str = _data['auth']['client_secret']
AUTH_ACCESS_TOKEN:str = _data['auth']['access_token']

BOT_MESSAGE:str = _data['bot']['message']
BOT_WELCOME:str = _data['bot']['welcome']
BOT_ADMINS:list = _data['bot']['admins']
BOT_LANG:str = _data['bot']['lang']

REDIS_HOST:str = _data['redis']['host']
REDIS_PORT:str = _data['redis']['port']

_raw_level = _data['log']['level']
LOG_LEVEL:int = logging.INFO
if _raw_level not in ("CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"):
    print(
        '#=> ERROR!!! Incorrect determination of the logging level '
        '"{_raw_level}". The level must be from the following list: '
        'CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET'
    )
    print('#=> The default logging level will be set to: INFO')
else:
    LOG_LEVEL = getattr(logging, _raw_level)
LOG_FORMAT:str = _data['log']['format']
LOG_DATE_FORMAT:str = _data['log']['date_format']

basicConfig(
    stream=sys.stdout,
    level=LOG_LEVEL,
    format=LOG_FORMAT,
    datefmt=LOG_DATE_FORMAT,
)
log = getLogger("autoresponder")
