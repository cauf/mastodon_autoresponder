import sys
import dbm

from typing import Any
from bs4 import BeautifulSoup
from mastodon import Mastodon
from redis import Redis
from settings import (
    log,
    AUTH_ACCESS_TOKEN,
    AUTH_CLIENT_ID,
    AUTH_CLIENT_SECRET,
    BASE_URL,
    LAST_ID_PATH,
    REDIS_HOST,
    REDIS_PORT,
    TOOT_LEN,
)

class APICallException(Exception):
    """
    Ошибка вызова API обращения к Mastodon
    """
    pass


def get_api() -> Mastodon:
    """
    Получение API обращения к Mastodon
    """    
    try:
        api = Mastodon(
            client_id = AUTH_CLIENT_ID,
            client_secret = AUTH_CLIENT_SECRET,
            access_token = AUTH_ACCESS_TOKEN,
            api_base_url = BASE_URL,
        )
    except Exception as ex:
        log.critical(
            'Error getting the Mastodon API. '
            'The service will be stopped.\n'
            f'{" Details ".center(80, "=")}\n{ex}\n'
            f'{"="*80}'
        )
        sys.exit(1)
    else:
        return api


def get_cache(db_num:int) -> Redis:
    """
    Получения коннектора к базам данных Redis

    :param db_num: номер базы данных
    :type db_num: int
    """    
    try:
        cache = Redis(
            host=REDIS_HOST,
            port=REDIS_PORT,
            db=db_num,
        )
    except Exception as ex:
        log.critical(
            'Error getting the Redis connection. '
            'The service will be stopped.\n'
            f'{" Details ".center(80, "=")}\n{ex}\n'
            f'{"="*80}'
        )
        sys.exit(1)
    else:
        return cache


def html_to_text(html:str) -> str:
    """
    Парсинг входящего html в простой текст

    :param html: Входящий html
    :type html: str
    :return: Содержимое html в виде простого текста
    :rtype: str
    """    
    soup = BeautifulSoup(html, 'html.parser')
    lines = []
    for p in soup('p'):
        lines.append(p.text)
    return '\n'.join(lines)


def read_last_id() -> int:
    """
    Считывает значение last_id из внешнего файла

    :return: значение id последнего обработанного уведомления
    :rtype: int
    """
    res = 0
    with dbm.open(str(LAST_ID_PATH), 'c') as lif:
        if 'last_id' in lif.keys():
            res = int(lif['last_id'])
    return res


def write_last_id(id:int) -> None:
    """
    Запись значения last_id во внешний файл

    :param id: значение id последнего обработанного уведомления
    :type id: int
    """    
    with dbm.open(str(LAST_ID_PATH), 'c') as lif:
        lif['last_id'] = str(id)
